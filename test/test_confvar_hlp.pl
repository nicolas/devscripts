# Helper for tests of Devscripts::Confvar.

# Copyright 2018 Nicolas Boulenguez <nicolas@debian.org>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

use strict;
use warnings;
use Devscripts::Confvar;

@Devscripts::Confvar::paths = $ARGV[0];

$ENV{from_env1} = '';
$ENV{from_env2} = '';
my $str_undefined       = undef;
my $str_default         = 'bla';
my $str_nondefault      = 'foo';
my $re_undefined        = undef;
my $re_default          = 'blue';
my $re_nondefault       = 'default';
my $bool_udfno          = undef;
my $bool_udfyes         = undef;
my $bool_0no            = 0;
my $bool_0yes           = 0;
my $bool_12no           = 12;
my $bool_12yes          = 12;
my @params              = ();
my $check_dirname_level = $Devscripts::Confvar::default_check_dirname_level;
my $check_dirname_regex = $Devscripts::Confvar::default_check_dirname_regex;
Devscripts::Confvar::parse(
    Devscripts::Confvar::str('from_env1', sub { die; }),
    Devscripts::Confvar::str('from_env2', sub { die; }),

    Devscripts::Confvar::str('str_unset', sub { die; }),
    Devscripts::Confvar::str(
        'str_escape',
        sub {
            die unless scalar @_ == 2;
            die unless shift eq 'str_escape';
            die unless shift eq 'bla bla\'bla"bla$bla\\bla\\nbla';
        }
    ),
    Devscripts::Confvar::str(
        'str_empty',
        sub {
            die unless scalar @_ == 2;
            die unless shift eq 'str_empty';
            die unless shift eq '';
        }
    ),
    Devscripts::Confvar::str('str_undefined',  \$str_undefined),
    Devscripts::Confvar::str('str_default',    \$str_default),
    Devscripts::Confvar::str('str_nondefault', \$str_nondefault),

    Devscripts::Confvar::str('re_unset',   sub { die; }, '^(red|blue)$'),
    Devscripts::Confvar::str('re_invalid', sub { die; }, '^(red|blue)$'),
    Devscripts::Confvar::str(
        're_callback',
        sub {
            die unless scalar @_ == 2;
            die unless shift eq 're_callback';
            die unless shift eq 'blue';
        },
        '^(red|blue)$'
    ),
    Devscripts::Confvar::str('re_undefined',  \$re_undefined,  '^(red|blue)$'),
    Devscripts::Confvar::str('re_default',    \$re_default,    '^(red|blue)$'),
    Devscripts::Confvar::str('re_nondefault', \$re_nondefault, '^(red|blue)$'),

    Devscripts::Confvar::bool('bool_unset',   sub { die; }),
    Devscripts::Confvar::bool('bool_invalid', sub { die; }),
    Devscripts::Confvar::bool(
        'bool_yes',
        sub {
            die unless scalar @_ == 2;
            die unless shift eq 'bool_yes';
            die unless shift;
        }
    ),
    Devscripts::Confvar::bool(
        'bool_no',
        sub {
            die unless scalar @_ == 2;
            die unless shift eq 'bool_no';
            die unless not shift;
        }
    ),
    Devscripts::Confvar::bool('bool_udfno',  \$bool_udfno),
    Devscripts::Confvar::bool('bool_udfyes', \$bool_udfyes),
    Devscripts::Confvar::bool('bool_0no',    \$bool_0no),
    Devscripts::Confvar::bool('bool_0yes',   \$bool_0yes),
    Devscripts::Confvar::bool('bool_12no',   \$bool_12no),
    Devscripts::Confvar::bool('bool_12yes',  \$bool_12yes),

    Devscripts::Confvar::wildcard(
        '^wildcard_[12]$',
        sub {
            die unless scalar @_ == 2;
            die
              unless ($_[0] eq 'wildcard_1' and $_[1] eq 'bla1')
              or ($_[0] eq 'wildcard_2' and $_[1] eq 'bla2');
        }
    ),

    Devscripts::Confvar::params(
        'params_callback',
        sub {
            die unless scalar @_ == 2;
            die unless shift eq 'params_callback';
            die unless shift =~ /^(a|b)$/;
        }
    ),
    Devscripts::Confvar::params('params', \@params),

    Devscripts::Confvar::check_dirname_level(\$check_dirname_level),
    Devscripts::Confvar::check_dirname_regex(\$check_dirname_regex),
);
# print "$Devscripts::Confvar::modified_conf_msg\n";
die unless $str_undefined eq 'bla';
die unless $str_default eq 'bla';
die unless $str_nondefault eq 'bla';
die unless $re_undefined eq 'blue';
die unless $re_default eq 'blue';
die unless $re_nondefault eq 'blue';
die unless not defined $bool_udfno;    # unchanged
die unless $bool_udfyes;               # reversed
die unless $bool_0no == 0;             # unchanged
die unless $bool_0yes;                 # reversed
die unless not $bool_12no;             # reversed
die unless $bool_12yes eq 12;          # unchanged
my @params_expected = ('bla', 'bla\'bla"bla$bla\\bla\\nbla');
die unless scalar @params == scalar @params_expected;

for (@params_expected) {
    die unless shift @params eq $_;
}
die unless $check_dirname_level == 2;
die unless $check_dirname_regex eq 'bla';
