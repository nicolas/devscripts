# Fake input for tests of Devscripts::Confvar.

# Copyright 2018 Nicolas Boulenguez <nicolas@debian.org>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

str_escape=bla\ bla\'bla\"bla\$bla\\bla\\nbla
str_empty=
str_undefined=bla
str_default=bla
str_nondefault=bla

re_invalid=blu
re_callback=blue
re_undefined=blue
re_default=blue
re_nondefault=blue

bool_invalid=true
bool_yes=yes
bool_no=no
bool_udfno=no
bool_udfyes=yes
bool_0no=no
bool_0yes=yes
bool_12no=no
bool_12yes=yes

wildcard_1=bla1
wildcard_2=bla2

params_callback=a\ b
params=$str_escape

DEVSCRIPTS_CHECK_DIRNAME_REGEX=bla
DEVSCRIPTS_CHECK_DIRNAME_LEVEL=2
