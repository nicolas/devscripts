# Parsing of configuration files for Debian devscripts tools.

# Copyright 2018 Nicolas Boulenguez <nicolas@debian.org>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

package Devscripts::Confvar;
use strict;
use warnings;
use Devscripts::Output qw(ds_error ds_warn);

###############
# Public part #
###############

# The main procedure is parse.
# It does not return anything, but fills the following variable with a
# log intended for insertion into the usage and help messages.
# The message extends on multiple lines, without final end of line
# character to make the insertion more natural.
our $modified_conf_msg;

# As described in devscripts.conf(5), if any key is defined in the
# environment, it is removed before processing the configuration files.

# Variable identifiers must match this regular expression.
my $re_identifier = qr'[A-Za-z](?:_?[A-Za-z0-9])*';

# The following functions construct arguments for parse.

# str ('key', \$variable)
# str ('key', \$variable, 'valid')
# str ('key', \&callback)
# str ('key', \&callback, 'valid')
# After execution of the configuration files, if a variable named
#   'key' exists, then its value is assigned to $variable.
# If a 'valid' regular expression is provided, only values matching it are
#   considered. The default value does not need to match.
# If a callback is provided, execute callback ('key', 'value') instead of
#   an assignment. No default value is displayed in the log.

# bool ('key', \&callback)
# bool ('key', \$variable)
# Similar to str with valid='^(no|yes)$, but
# * interpret the previous content of $variable as a perl boolean
#   (and die if it is 'yes' or 'no', 'no' meaning true).
# * assign a perl boolean to $variable, but only if its boolean value
#   must change (replacing 0 or undef with '' in the back of the
#   caller would only cause problems).
# * in the second form, pass a boolean second parameter.

# wildcard ('pattern', \&callback)
# Whenever key=value with 'key' matching 'pattern', execute
#   callback ('key', 'value');
# Any matching key in the environment is unset before processing.

# params ('pattern', \&callback)
# params ('key', \@variable)
# If key exists in the configuration files, its value is expanded in the
# shell context into a word list, then for each word execute
#   callback ('key', 'value')
# The callback will be called many times with the same key.
# The second form is equivalent to
#   params ('key', sub { push @variable, $_[1]; });

# Shortcuts for global variables described in devscripts.conf(5).
sub check_dirname_level {
    return str('DEVSCRIPTS_CHECK_DIRNAME_LEVEL', @_, '^[012]$');
}

sub check_dirname_regex {
    return str('DEVSCRIPTS_CHECK_DIRNAME_REGEX', @_);
}
# Help providing consistent defaults across tools:
our $default_check_dirname_level = 1;
our $default_check_dirname_regex = 'PACKAGE(-.+)?';

# Convenient callback for shell scripts.
sub sh_set {
    my ($key, $value) = @_;
    $value =~ s/'/'\\''/g;
    print "$key='$value'\n";
}

##################
# Implementation #
##################

# Configuration files.
our @paths = (    # "our" is only intended for tests
    '/etc/devscripts.conf',
    (defined $ENV{'HOME'} ? "$ENV{'HOME'}/.devscripts" : ()),
);
# Compile once and for all these expressions.
my $re_identifier_alone = qr/^$re_identifier$/;
my $re_assign_blank     = qr/^\s*($re_identifier)=\s+[^#\s]/;
my $re_only_assign
  = qr/^\s*(?:$re_identifier=(?:[^&();|<>\s\$"\\'[\]]*|'[^']*'|"[^\$\`"\\]*"))?\s*(?:#.*)?\n$/;
my $re_bool = qr/^(no|yes)$/;

# Separate log stuff.
sub _log_start {
    $modified_conf_msg = undef;
}

sub _log {
    $modified_conf_msg .= "\n  $_[0]";
}

sub _log_end {
    $modified_conf_msg ||= "\n  (none)";
    $modified_conf_msg
      = 'Default settings modified by devscripts configuration files:'
      . $modified_conf_msg;
}

# Each argument constructor returns a hash containing these keys.
# * key_pattern               (regular expression)
#   Matching keys in the environment are unset before processing.
#   Matching keys in the shell after processing are passed to:
# * process ('key', 'value')  (procedure)
# * after_conf                (string)
#   Shell commands executed after configuration files.
#   Inexistant, undefined and empty are equivalent.

# Use closures instead of passing the object as first parameter to its
# methods as usual.  This simplifies the source but duplicates some
# code for every instance. Changing this would be straightforward.

sub str {
    die unless scalar @_ == 2 or scalar @_ == 3;
    die unless $_[0] =~ $re_identifier_alone;
    my $ref   = $_[1];
    my $valid = $_[2];    # may be undefined.
    return {
        key_pattern => qr/^$_[0]$/,
        process     => sub {
            my ($key, $value) = @_;
            if ($valid and $value !~ $valid) {
                _log "$key is ignored ($value should match $valid)";
                ds_warn "$key is ignored ($value should match $valid)";
            } elsif (ref $ref eq 'CODE') {
                &$ref(@_);
                _log "$key=$value";
            } elsif (ref $ref ne 'SCALAR') {
                die;
            } elsif (not defined $$ref) {
                $$ref = $value;
                _log "$key=$value (previously undefined)";
            } elsif ($value eq $$ref) {
                _log "$key=$value (which was the default)";
            } else {
                $$ref = $value;
                _log "$key=$value (previously $$ref)";
            }
        },
    };
}

sub bool {
    die unless scalar @_ == 2;
    die unless $_[0] =~ $re_identifier_alone;
    my $ref = $_[1];
    die if ref $ref eq 'SCALAR' and defined $$ref and $$ref =~ $re_bool;
    return {
        key_pattern => qr/^$_[0]$/,
        process     => sub {
            my ($key, $value) = @_;
            if ($value !~ $re_bool) {
                _log "$key is ignored ($value should be yes or no)";
                ds_warn "$key is ignored ($value should be yes or no)";
            } elsif (ref $ref eq 'CODE') {
                &$ref($key, $value ne 'no');
                _log "$key=$value";
            } elsif (ref $ref ne 'SCALAR') {
                die;
            } elsif ($$ref xor $value eq 'no') {
                _log "$key=$value (which was the default)";
            } else {
                $$ref = $value ne 'no';
                _log "$key=$value (changing the default)";
            }
        },
    };
}

sub wildcard {
    die unless scalar @_ == 2;
    my $ref = $_[1];
    return {
        key_pattern => qr/$_[0]/,
        process     => sub {
            my ($key, $value) = @_;
            &$ref(@_);
            _log "$key=$value";
        },
    };
}

sub params {
    die unless scalar @_ == 2;
    die unless $_[0] =~ $re_identifier_alone;
    my $ref = $_[1];
    return {
        key_pattern => qr/^$_[0]$/,
        after_conf  => <<"EOF",
for v in \$$_[0]; do
    echo -n $_[0]=\\'
    echo -n \$v | sed s/\\'/\\\'\\\\\\\\\\'\\'/g
    echo \\'
done
unset v $_[0]
EOF
        process => sub {
            my ($key, $value) = @_;
            if (ref $ref eq 'ARRAY') {
                push @$ref, $value;
            } elsif (ref $ref eq 'CODE') {
                &$ref(@_);
            } else {
                die;
            }
            _log "$key=$value";
        },
    };
}

# Parse procedure.
sub parse {
    _log_start;
    if (@ARGV and $ARGV[0] =~ '^--no-?conf$') {
        shift @ARGV;
        _log '(no configuration files read)';
    } else {

        # A clean environment is required by devscripts.conf(5), and
        # simplifies a lot the distinction between an unset variable and a
        # variable set with its default value.
        # A walk through %ENV is necessary because of wildcards. Sparing
        # it in absence of any wildcard seems overkill.
        my $shell_cmd = '';
        foreach my $key (keys %ENV) {
            if ($key =~ $re_identifier_alone) {
                # Never put strange characters into shell_cmd.
                foreach (@_) {
                    if ($key =~ $$_{key_pattern}) {
                        $shell_cmd .= " $key";
                        last;
                    }
                }
            }
        }
        if ($shell_cmd) {
            $shell_cmd = 'unset' . $shell_cmd . "\n";
        }

        my $only_assign = 1;
        foreach my $file (@paths) {
            if (-e $file) {

                open(my $fh, '<', $file) or die;
                while (defined(my $line = <$fh>)) {
                    if ($only_assign and $line !~ $re_only_assign) {
                        # This cannot be printed as long as our output
                        # is consumed by shell scripts.
                        ds_warn <<"EOF";
devscripts configuration files should only contain simple assignments.
This may become a requirement in future versions.
EOF
                        $only_assign = '';
                    }
                    if ($line =~ $re_assign_blank) {
                        ds_error <<"EOF";
In $file, an assignment is followed by a space.
This implies that the shell will execute the rest of the line,
which is probably not what you expect in a configuration file.
Please use an explicit 'unset $1' or $1='' instead.
EOF
                    }
                }
                close $fh or die;

                if (system '/bin/bash', '-n', $file) {
                    ds_error "bash -n reports a syntax error in file '$file'.";
                }

                $shell_cmd .= ". '$file'\n";
            }
        }

        foreach (@_) {
            if ($$_{after_conf}) {
                $shell_cmd .= $$_{after_conf};
            }
        }

        $shell_cmd .= "set\n";

        # print "--\n$shell_cmd--\n";

        # devscripts.conf(1) requires /bin/bash.
        # -e always seems appropriate for a configuration file.
        # To debug, add -v or -x.
        $shell_cmd =~ s/'/'\\''/g;
        my $output = `/bin/bash -ce '$shell_cmd'`;
        if (not $output) {
            ds_error "Error while executing '$shell_cmd': $!";
        }

        # print "-- output:\n$output--\n";

        # Each line has the form key=value.
        # * key is a shell identifier.
        # We only consider those maching our convention, not '_' for example.
        # Values may be represented in 4 formats.
        # * a plain shell word.
        # * a string between single quotes. The only character that cannot
        #   be represented is ', so bash uses a '\'' concatenation.
        # * a dollar followed by a string between single quotes, which is used
        #   to represent control characters by ANSI sequences like \n.
        # * a bash array variable, between parenthesis.
        # We only handle the two first formats.
        my $splitter = qr/($re_identifier)=(?:([^\n'\$(]*)|'([^\n]*)')\n/;
        while ($output =~ /$splitter/g) {
            my $key = $1;
            my $value;
            if (defined $2) {
                $value = $2;
            } else {
                $value = $3;
                $value =~ s/'\\''/'/g;
            }
            foreach (@_) {
                if ($key =~ $$_{key_pattern}) {
                    &{ $$_{process} }($key, $value);
                    last;
                }
            }
        }
    }
    _log_end;
}

1;    # Module correctly initialized.
