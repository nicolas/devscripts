#!/bin/bash

set -e

PROGNAME=`basename $0`

usage () {
    echo \
"Usage: $PROGNAME [options]
  Clean all debian build trees under current directory.

  Options:
    --cleandebs    Also remove all .deb, .changes and .build
                   files from the parent of each build tree

    --nocleandebs  Don't remove the .deb etc. files (default)

    --check-dirname-level N
                   How much to check directory names before cleaning trees:
                   N=0   never
                   N=1   only if program changes directory (default)
                   N=2   always

    --check-dirname-regex REGEX
                   What constitutes a matching directory name; REGEX is
                   a Perl regular expression; the string \`PACKAGE' will
                   be replaced by the package name; see manpage for details
                   (default: 'PACKAGE(-.+)?')

    --no-conf, --noconf
                   Do not read devscripts config files;
                   must be the first option given

    -d             Do not run dpkg-checkbuilddeps to check build dependencies

    --help         Display this help message and exit

    --version      Display version information

$MODIFIED_CONF_MSG"
}

version () {
    echo \
"This is $PROGNAME, from the Debian devscripts package, version ###VERSION###
This code is copyright 1999 by Julian Gilbey, all rights reserved.
Original code by Christoph Lameter.
This program comes with ABSOLUTELY NO WARRANTY.
You are free to redistribute this code under the terms of the
GNU General Public License, version 2 or later."
}

if [ "$1" = "--no-conf" -o "$1" = "--noconf" ]; then
    shift
    confvar_no=--\ -\ --noconf
fi
eval "$(perl $confvar_no <<EOF
use strict;
use warnings;
use Devscripts::Confvar;
my \$c = 'no';
my \$l = \$Devscripts::Confvar::default_check_dirname_level;
my \$r = \$Devscripts::Confvar::default_check_dirname_regex;
Devscripts::Confvar::parse (
    Devscripts::Confvar::str ('DEBCLEAN_CLEANDEBS', \\\$c, '^(no|yes)\$'),
    Devscripts::Confvar::check_dirname_level        (\\\$l),
    Devscripts::Confvar::check_dirname_regex        (\\\$r),
    );
Devscripts::Confvar::sh_set 'DEBCLEAN_CLEANDEBS',  \$c;
Devscripts::Confvar::sh_set 'CHECK_DIRNAME_LEVEL', \$l;
Devscripts::Confvar::sh_set 'CHECK_DIRNAME_REGEX', \$c;
Devscripts::Confvar::sh_set 'MODIFIED_CONF_MSG',   \$Devscripts::Confvar::modified_conf_msg;
EOF
)"

# Need -o option to getopt or else it doesn't work
TEMP=$(getopt -s bash -o "" -o d \
	--long cleandebs,nocleandebs,no-cleandebs \
	--long no-conf,noconf \
	--long check-dirname-level:,check-dirname-regex: \
	--long help,version -n "$PROGNAME" -- "$@")
if [ $? != 0 ] ; then exit 1 ; fi

eval set -- $TEMP

# Process Parameters
while [ "$1" ]; do
    case $1 in
    --cleandebs) DEBCLEAN_CLEANDEBS=yes ;;
    --nocleandebs|--no-cleandebs) DEBCLEAN_CLEANDEBS=no ;;
    --check-dirname-level)
	shift
        case "$1" in
	0|1|2) CHECK_DIRNAME_LEVEL=$1 ;;
	*) echo "$PROGNAME: unrecognised --check-dirname-level value (allowed are 0,1,2)" >&2
	   exit 1 ;;
        esac
	;;
    -d)
    	CHECKBUILDDEP="-d" ;;
    --check-dirname-regex)
	shift; 	CHECK_DIRNAME_REGEX="$1" ;;
    --no-conf|--noconf)
	echo "$PROGNAME: $1 is only acceptable as the first command-line option!" >&2
	exit 1 ;;
    --help) usage; exit 0 ;;
    --version) version; exit 0 ;;
    --)	shift; break ;;
    *) echo "$PROGNAME: bug in option parser, sorry!" >&2 ; exit 1 ;;
    esac
    shift
done

# Still going?
if [ $# -gt 0 ]; then
    echo "$PROGNAME takes no non-option arguments;" >&2
    echo "try $PROGNAME --help for usage information" >&2
    exit 1
fi


# Script to clean up debian directories

OPWD="`pwd`"

TESTDIR=$(echo $OPWD | egrep -o '.*/debian/?' | sed 's/\/debian\/\?$//')

if [ -f debian/changelog ]; then
    directories=$OPWD
elif [ -f "$TESTDIR/debian/changelog" ]; then
    directories=$TESTDIR
else
    directories=$(find . -type d -name "debian" -a ! -wholename '*.git*/debian')
fi

for i in $directories; do
    (  # subshell to not lose where we are
    DIR=${i%/debian}
    echo "Cleaning in directory $DIR"
    cd $DIR

    # Clean up the source package, but only if the directory looks like
    # a genuine build tree
    if [ ! -f debian/changelog ]; then
	echo "Directory $DIR: contains no debian/changelog, skipping" >&2
	exit
    fi
    package="`dpkg-parsechangelog -SSource`"
    if [ -z "$package" ]; then
	echo "Directory $DIR: unable to determine package name, skipping" >&2
	exit
    fi

    # let's test the directory name if appropriate
    if [ $CHECK_DIRNAME_LEVEL -eq 2 -o \
	\( $CHECK_DIRNAME_LEVEL -eq 1 -a "$OPWD" != "`pwd`" \) ]; then
	if ! perl -MFile::Basename -w \
	    -e "\$pkg='$package'; \$re='$CHECK_DIRNAME_REGEX';" \
	    -e '$re =~ s/PACKAGE/\\Q$pkg\\E/g; $pwd=`pwd`; chomp $pwd;' \
	    -e 'if ($re =~ m%/%) { eval "exit (\$pwd =~ /^$re\$/ ? 0:1);"; }' \
	    -e 'else { eval "exit (basename(\$pwd) =~ /^$re\$/ ? 0:1);"; }'
	then
	    echo "Full directory path `pwd` does not match package name, skipping." >&2
	    echo "Run $progname --help for more information on directory name matching." >&2
	    exit
	fi
    fi

    # We now know we're OK and debuild won't complain about the dirname
    debuild $CHECKBUILDDEP -- clean

    # Clean up the package related files
    if [ "$DEBCLEAN_CLEANDEBS" = yes ]; then
	cd ..
	rm -f *.changes *.deb *.build
    fi
    )
done
